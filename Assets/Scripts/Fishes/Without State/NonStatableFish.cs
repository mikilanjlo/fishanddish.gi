﻿using UnityEngine;

public class NonStatableFish : MonoBehaviour
{
    #region Fields
    [SerializeField] SOFish m_fishConfig;

    [Header("Settings for debug:")]
    [SerializeField] private bool useRoute = true;
    [SerializeField] private bool useForce = true;

    private MotionManager motion;

    //private AState m_curState;
    #endregion

    //public float PunchAbility { get { return m_fishConfig.PunchAbility; } }

    public float Health { get; set; }

    public bool IsAlive { get { return Health > 0; } }
    
    #region Unity
    private void Start()
    {
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        renderer.sprite = m_fishConfig.BodySprite;

        //Health = m_fishConfig.Health;

        //TODO выборка по энаму конкретной рыбы и навесить в соответствии на рыбу необходимые скрипты

        //TODO State
        //m_curState = new IdleState(this);

        //object with motion Functions
        motion = new MotionManager(this);
        //function create Route
        motion.CreateRoute();
    }


    private void Update () 
    {
        //m_curState.TryToChangeState()

        //m_curState.Move();

        motion.Update();
    }

    #endregion

    #region IBoid

    public bool GetEndLife()
    {
        return false;
    }

    public bool GetUseForce()
    {
        return useForce;
    }

    public bool GetUseRoute()
    {
        return useRoute;
    }

    public Transform GetTransform()
    {
        return transform;
    }
    #endregion
}