﻿using System.Collections.Generic;
using UnityEngine;

public class Route  
{
    #region Fields
    public static int segmentCount = 10; // number of points on (detalization of Bezier curve)
    
    float blindZone = 5f; // offset for out-of-screen area, where fish can still swim

    float heightMap;
    float weightMap;

    // 2 corners of the map 
    Vector3 maxBound; // - ♥
    Vector3 minBound; // ♥ -
    
    Vector3 endPos; // last point in route (Beizer curve)
    Queue<Vector3> points; // route's (Beizer curve's) points
    #endregion

    #region Constructor
    public Route()
    {
        endPos = new Vector3(15, 15, 0);
        points = new Queue<Vector3>();
    }
    #endregion

    #region Public methods
    /// <summary>
    /// Return target or Last "dead" position
    /// </summary>
    /// <returns>target or Last "dead" position</returns>
    public Vector3 GetTarget(bool dead)
    {
        if (dead)
        {
            return endPos;
        }
        if (points.Count < 4)
            BezierRoute(1);
        if (points.Count > 0)
            return points.Dequeue();
        else
        {
            BezierRoute(1);
            return points.Dequeue();
        }
    }


    /// <summary>
    /// Run with object's Start() and Initialize variables
    /// </summary>
    public void BezierRoute(int numberLine = 4)
    {
        CreateBounds();

        Vector3 p0 = new Vector3(0, 0);

        for (int i = 0; i < numberLine; i++)
        {
            Vector3 p1 = RandomPoint(); 
            Vector3 p2 = RandomPoint();
            Vector3 p3 = RandomPoint();
            CreatePointsBezier(p0, p1, p2, p3);
            p0 = p3;
        }
    }

    
    /// <summary>
    /// Creates and returns point in the blind area behind the screen for making fish go away
    /// </summary>
    public Vector3 RandomEndPoint()
    {
        Vector3 point = new Vector3();

        // 5f is an extra points so fish will be in out-of-screen area completely

        point.x = Random.value * blindZone - blindZone * .5f;
        point.x = point.x < 0 ? minBound.x - point.x - 5f : maxBound.x + point.x + 5f; 

        point.y = Random.value * blindZone - blindZone * .5f;
        point.y = point.y < 0 ? minBound.y - point.y - 5f : maxBound.y + point.y + 5f; 

        return point;
    }
    #endregion
    
    #region Private methods
    /// <summary>
    /// Set bounds of the map 
    /// </summary>
    void CreateBounds()
    {
        // sets 2 corners of the map 
        maxBound = Camera.main.ViewportToWorldPoint(new Vector2(1, 1)); // - ♥
        minBound = Camera.main.ViewportToWorldPoint(new Vector2(0, 0)); // ♥ -   

        // sets size of map
        weightMap = maxBound.x - minBound.x;
        heightMap = maxBound.y - minBound.y;
    }

    
    /// <summary>
    /// Create and return new random point in the screen area
    /// </summary>
    Vector3 RandomPoint()
    {
        Vector3 point = new Vector3();

        point.x = minBound.x + Random.value * weightMap + blindZone;
        point.y = minBound.y + Random.value * heightMap + blindZone;

        return point;
    }    


    /// <summary>
    /// Find Bezier curve 
    /// </summary>
    /// <param name="p0"></param>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <param name="p3"></param>
    void CreatePointsBezier(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float t = 0;
        for (int i = 1; i <= segmentCount; ++i)
        {
            t += 1 / (float)segmentCount;
            Vector3 pp = CalculateBezierPoint(t, p0, p1, p2, p3);
            points.Enqueue(pp);
        }
    }


    /// <summary>
    /// Find and return Bezier point
    /// </summary>
    /// <param name="t"></param>
    /// <param name="p0"></param>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <param name="p3"></param>
    /// <returns></returns>
    Vector3 CalculateBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector3 p = uuu * p0;    //first term
        p += 3 * uu * t * p1;    //second term
        p += 3 * u * tt * p2;    //third term
        p += ttt * p3;           //fourth term

        return p;
    }


    /// <summary>
    /// Find and return average Distance. Adds all distances and divides them by their number
    /// </summary>
    /// <returns>Average Distance</returns>
    float FindAverageDistance()
    {
        float sumDistance = 0;
        bool initializePoint1 = true;
        Vector3 point1 = new Vector3();
        foreach (Vector3 point2 in points)
        {
            if (initializePoint1)
            {
                point1 = point2;
                initializePoint1 = !initializePoint1;
            }
            else
            {
                sumDistance += Vector3.Distance(point1, point2);
                point1 = point2;
            }
        }
        return sumDistance / (points.Count - 1);
    }


    private void EditRoute()
    {
        bool rightPoint = false;
        foreach(Vector3 point in points)
        {
            if (point.x < (minBound.x + weightMap / 10))
                rightPoint = true;
        }
        //if(!rightPoint)
        //    Random.value * points.Count
    }
    #endregion
}
