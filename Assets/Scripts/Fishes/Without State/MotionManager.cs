﻿using UnityEngine;

public class MotionManager  
{
    #region Fields
    public static float rotationSpeed = 1f;

    private NonStatableFish host;
    Motion motion;
    Route route;
    Vector3 velocity;
    Vector3 startPos;
    Vector3 target;
    bool motionAlternetion;
    FishRotation fishRotation;
    #endregion
    
    #region Contructor
    public MotionManager(NonStatableFish host)
    {
        //link to our variables from fish
        this.host = host;
        motion = new Motion();
        route = new Route();
        startPos = host.GetTransform().position;
        target = startPos;
        velocity = Vector3.zero;
        //motionAlternetion = false;
        //fishRotation = new FishRotation();
    }
    #endregion

    #region Unity
    /// <summary>
    /// update position
    /// </summary>
    public void Update()
    {
        //if (host.GetUseForce())
        //{
        //    if (!host.GetUseRoute())
        //        WanderWithoutTargetForceMovement();
        //    else
        //        WanderTargetForceMovement();
        //}
        //else
        //{
        //    WanderTargetAlternetionMovement();
        //}
        WanderTargetForceMovement();
    }
    #endregion

    #region Public methods
    /// <summary>
    /// Create route (Beizer curve)
    /// </summary>
    public void CreateRoute()
    {
        route.BezierRoute();
    }


    
    #endregion

    #region Private methods
    ///// <summary>
    ///// Alternetion Rotation and Movement along the route
    ///// </summary>
    //void WanderTargetAlternetionMovement()
    //{
    //    // Rotation
    //    if (!motionAlternetion)
    //    {
    //        fishRotation.RotationAroundObject(startPos, target, host.GetTransform(), rotationSpeed, ref motionAlternetion);
    //        velocity = target - host.GetTransform().position;
    //    }
    //    // Movement
    //    else
    //    {
    //        float step = 0.1f;
    //        motion.Wander(ref velocity, host.GetTransform(), step);
    //    }
    //    // Check if the target is reached
    //    if (Vector3.Distance(host.GetTransform().position, target) < 0.2 * 2)
    //    {
    //        motionAlternetion = false;
    //        startPos = host.GetTransform().position;
    //        target = route.GetTarget(host.GetEndLife());
    //    }
    //}


    /// <summary>
    /// Move fish along the route with Force 
    /// </summary>
    void WanderTargetForceMovement()
    {
        //max step for motion
        float step = 0.1f;
        //our motion
        motion.Wander(target,ref velocity, host.GetTransform(), step);
        //we check reached the target
        if (Vector3.Distance(host.GetTransform().position, target) < step * 2)
        {
            //change start to our position  and change now target to next target
            startPos = host.GetTransform().position;
            target = route.GetTarget(host.GetEndLife());
        }
            
    }


    ///// <summary>
    ///// Movement with force without route
    ///// </summary>
    //void WanderWithoutTargetForceMovement()
    //{
    //    float step = 0.1f;

    //    motion.Wander(ref velocity, host.GetTransform(), step);
    //}


    //bool IsPointReached(Vector3 vector1, Vector3 vector2, float radius)
    //{
    //    return Vector3.Distance(vector1, vector2) < radius;
    //}   
    #endregion
}
