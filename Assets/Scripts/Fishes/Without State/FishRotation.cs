﻿using UnityEngine;

public class FishRotation  
{
    #region Fields
    bool startRotation;
    GameObject ob;
    Vector3 startEuler;
    float angle;
    float time;
    float rotationTime;
    #endregion

    #region Constructor
    public FishRotation()
    {
        startRotation = true;
    }
    #endregion

    #region  Public methods
    public void SimpleRotation(Transform present)
    {
        present.eulerAngles = Vector3.Lerp(startEuler, new Vector3(0, 0, angle), rotationTime / time);
    }


    public void RotationAroundObject(Vector3 startPos, Vector3 endPos,Transform present,float rotationSpeed, ref bool motion)
    {
        if (startRotation)
        {
            Vector3 dir = endPos - startPos;
            ob = new GameObject(); ;
            ob.transform.position = present.position;
            ob.transform.rotation = present.rotation;
            ob.transform.up = dir;
            angle = ob.transform.eulerAngles.z;
            Vector3 realativePosition = present.position;
            ob.transform.eulerAngles = present.eulerAngles;
            float indentDistance = 2;
            if (Mathf.Abs(angle - present.eulerAngles.z) < 45)
            {
                EndRotationArounObject(present, ref motion);
                return;
            }
            if (angle < present.eulerAngles.z)
            {
                ob.transform.position = present.TransformPoint(Vector3.right * (indentDistance));
            }
            if (angle > present.eulerAngles.z)
            {
                ob.transform.position = present.TransformPoint(Vector3.right * -(indentDistance));
            }
            present.parent = ob.transform;
            startEuler = ob.transform.eulerAngles;
            time = rotationSpeed / 180 * Mathf.Abs(angle - startEuler.z);
            rotationTime = 0;
        }
        startRotation = false;
        SimpleRotation(ob.transform);
        rotationTime += Time.deltaTime;
        if (rotationTime > time)
        {
            EndRotationArounObject(present,ref motion);
        }
    }
    #endregion

    #region Private methods
    void EndRotationArounObject(Transform present,ref bool motion)
    {
        present.parent = null;
        Object.Destroy(ob);
        startRotation = true;
        motion = true;
    }
    #endregion
}
