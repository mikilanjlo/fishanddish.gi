﻿using UnityEngine;

public class Motion
{
    #region Fields
    //private int changeAngle = 100; // changes wanderAngle every {changeAngle} time
    //private int changeAngleTime = 1; // what time it is now (enumerator)
    private float wanderAngle; // angle(WanderForce | Current Force)]
    //private float curAngle = 0f;
    #endregion

    #region Constructor
    public Motion()
    {
        this.wanderAngle = 0;
    }
    #endregion

    #region Public methods
    ///// <summary>
    ///// overload of Wander() for movement without route
    ///// </summary>
    ///// <param name="velocity"></param>
    ///// <param name="present">transform our bot</param>
    ///// <param name="step">//max step for motion</param>
    //public void Wander(ref Vector3 velocity, Transform present, float step)
    //{
    //    this.Wander(ref velocity, present, step, false);
    //}


    /// <summary>
    /// Movement with force
    /// </summary>
    /// <param name="target"></param>
    /// <param name="velocity"></param>
    /// <param name="present">transform our bot</param>
    /// <param name="step">max step for motion</param>
    /// <param name="useRoute">use or don't use Route</param>
    public void Wander(Vector3 target ,ref Vector3 velocity, Transform present, float step, bool useRoute = true)
    {

        //find force
        Vector3 steering = WanderForce(target, present.position, velocity);//useRoute ? this.WanderForce(target, present.position, velocity) : this.WanderCircleForce(velocity);
        //devision by the number of vectors in the path
        steering /= 50;
        //gaining velocity and format in max step
        velocity = Truncate(velocity + steering, step);
        //change position
        present.position += velocity;
        present.up = velocity;
    }
    #endregion

    #region Private methods

    ///// <summary>
    ///// Movement along the line
    ///// </summary>
    ///// <param name="present">transform our bot</param>
    ///// <param name="startPos">start movement position</param>
    ///// <param name="endPos">end movement position</param>
    ///// <param name="time">time of movement</param>
    ///// <param name="curTime">past time</param>
    //private void SimpleMotion(Transform present, Vector3 startPos, Vector3 endPos, float time, float curTime)
    //{
    //    //change position by segments
    //    Vector3 willPosition = Vector3.Lerp(startPos, endPos, curTime / time);
    //    present.position = willPosition;
    //}


    /// <summary>
    /// Find force aplied to component
    /// </summary>
    /// <param name="target">End Point</param>
    /// <returns>Force</returns>
    private Vector3 WanderForce(Vector3 target, Vector3 presentPosition, Vector3 velocity)
    {
        Vector3 desiderVelocity = target - presentPosition;
        return desiderVelocity - velocity;
    }

    private Vector3 Truncate(Vector3 vector, float max)
    {
        if (vector.magnitude > max)
        {
            float deviderStep = (vector.magnitude / max);
            return vector / deviderStep;
        }
        return vector;
    }


    ///// <summary>
    ///// Find and return force using circle example on the site
    ///// </summary>
    ///// <param name="velocity"></param>
    ///// <returns></returns>
    //private Vector3 WanderCircleForce(Vector3 velocity)
    //{
    //    float circleRadius = 0.5f;
    //    float circleDistance = 0.2f + circleRadius;
    //    float angleChance = 1f / this.changeAngle; //0.05f;
    //    int maxAngleChange = 300;

    //    // find circle's center
    //    Vector3 circleCenter = new Vector3(velocity.x, velocity.y);
    //    circleCenter = this.ScaleBy(circleCenter, circleDistance);

    //    // find displacement force
    //    Vector3 displacement = new Vector3(0, -1);
    //    displacement = this.ScaleBy(displacement, circleRadius);

    //    this.curAngle += this.wanderAngle;
    //    this.curAngle %= 180f;

    //    // turn in random way displacement force with adding the wanderAngle 
    //    displacement = this.SetAngle(displacement, this.curAngle);

    //    if (this.changeAngleTime++ == this.changeAngle)
    //    {
    //        this.changeAngleTime = 1;
    //        this.changeAngle = (int)(UnityEngine.Random.value * maxAngleChange);

    //        this.wanderAngle = UnityEngine.Random.value * angleChance - angleChance * .5f;
    //    }

    //    // calculate and return wanderForce
    //    Vector3 wanderForce = circleCenter + displacement;
    //    return wanderForce;
    //}

    //private Vector3 ScaleBy(Vector3 vector, float max)
    //{
    //    return this.Truncate(vector, vector.magnitude * max);
    //}



    //private Vector3 SetAngle(Vector3 vector, float value)
    //{
    //    float len = vector.magnitude;
    //    vector.x = Mathf.Cos(value) * len;
    //    vector.y = Mathf.Sin(value) * len;
    //    return vector;
    //}
    #endregion
}
