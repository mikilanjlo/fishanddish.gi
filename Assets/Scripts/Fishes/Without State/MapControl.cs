﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapControl : MonoBehaviour
{
    #region Fields
    //fish prefab
    public GameObject baseFish;
    #endregion

    #region Unity
    void Start()
    {
        Createfish();
    }    
    #endregion

    #region Public methods
    public void Createfish()
    {
        Vector3 pos = RandomOutOfScreenPoint();

        GameObject currentDecal = Instantiate<GameObject>(baseFish);
        currentDecal.transform.position = pos;
    }


    /// <summary>
    /// Creates and returns point in the blind area behind the screen for making fish go away
    /// </summary>
    public Vector3 RandomOutOfScreenPoint()
    {
        float blindZone = 5f;
        Vector3 point = new Vector3();

        // sets 2 corners of the map 
        Vector3 maxBound = Camera.main.ViewportToWorldPoint(new Vector2(1, 1)); // - ♥
        Vector3 minBound = Camera.main.ViewportToWorldPoint(new Vector2(0, 0)); // ♥ -   

        // 5f is an extra points so fish will be in out-of-screen area completely

        point.x = Random.value * blindZone - blindZone * .5f;
        point.x = point.x < 0 ? minBound.x - point.x - 5f : maxBound.x + point.x + 5f; 

        point.y = Random.value * blindZone - blindZone * .5f;
        point.y = point.y < 0 ? minBound.y - point.y - 5f : maxBound.y + point.y + 5f; 

        return point;
    }  
    #endregion 
}
