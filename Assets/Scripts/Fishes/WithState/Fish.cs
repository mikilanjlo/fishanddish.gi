﻿using Assets.Scripts.Fishes.WithState;
using UnityEngine;

public class Fish : MonoBehaviour
{
    #region Fields
    [SerializeField] private SOFish m_fishConfig;

    private SpriteRenderer m_renderer;
    private Sprite m_aliveFishSprite;
    private Sprite m_deadFishSprite;

    private CapsuleCollider2D m_collider;
    #endregion

    #region Properties
    public float Health { get; set; }
    public AState CurState { get; set; }

    public float PunchAbility { get { return this.m_fishConfig.PunchAbility; } }
    public bool IsAlive { get { return this.Health > 0; } }

    public EFishType FishType { get; private set; }
    public EFishSize FishSize { get; private set; }

    public float Cost { get; private set; }
    #endregion

    #region MonoBehaviour
    private void Start()
    {
        if (this.m_fishConfig == null)
        {
            Debug.LogError("Fish config is missing!");
            return;
        }

        this.m_renderer = this.GetComponent<SpriteRenderer>();
        this.m_aliveFishSprite = this.m_fishConfig.BodySprite;
        this.m_deadFishSprite = this.m_fishConfig.DeadSprite;

        this.m_collider = gameObject.AddComponent<CapsuleCollider2D>();
        this.m_collider.direction = CapsuleDirection2D.Horizontal;

        this.Health = this.m_fishConfig.Health;

        this.FishType = this.m_fishConfig.Type;
        this.FishSize = this.m_fishConfig.Size;
        switch (this.m_fishConfig.Type)
        {
            case EFishType.Base:
                {
                    break;
                }
            case EFishType.Predator:
                {
                    // TODO Add script eatting other fish
                    break;
                }
            case EFishType.Oxygen:
                {
                    // TODO Add script to collect oxygen
                    break;
                }
            case EFishType.Jellyfish:
                {
                    // TODO Add script to hit oxygen
                    break;
                }
            case EFishType.Octopus:
                {
                    // TODO Add script to hit oxygen
                    break;
                }
        }

        this.CurState = new IdleState(this);

        this.Cost = this.m_fishConfig.Cost;
    }


    private void Update()
    {
        this.CurState.TryToChangeState();

        this.CurState.Motion();
    }

    public void SetAliveFishSprite()
    {
        this.m_renderer.sprite = this.m_aliveFishSprite;
    }

    public void SetDeadFishSprite()
    {
        this.m_renderer.sprite = this.m_deadFishSprite;
    }

    #endregion
}
