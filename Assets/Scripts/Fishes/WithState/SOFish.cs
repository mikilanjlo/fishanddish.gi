﻿using Assets.Scripts.Fishes.WithState;
using UnityEngine;

[CreateAssetMenu(fileName = "NewFishConfig", menuName ="Fishes/DefaultFish")]
public class SOFish : ScriptableObject
{
    [SerializeField] private float m_punchAbility;
    [SerializeField] private float m_health;
    [SerializeField] private float m_movingSpeed;
    [SerializeField] private float m_rotatingSpeed;
    [SerializeField] private EFishType m_type;
    [SerializeField] private EFishSize m_size;
    [SerializeField] private Sprite m_bodySprite;
    [SerializeField] private Sprite m_deadSprite;
    [SerializeField] private float m_cost;

    public float PunchAbility { get { return m_punchAbility; } }
    public float Health { get { return m_health; } }
    public float MovingSpeed { get { return m_movingSpeed; } }
    public float RotatingSpeed { get { return m_rotatingSpeed; } }
    public EFishType Type { get { return m_type; } }
    public EFishSize Size { get { return m_size; } }

    public Sprite BodySprite { get { return m_bodySprite; } }
    public Sprite DeadSprite { get { return m_deadSprite; } }

    public float Cost { get { return this.m_cost; } }
}
