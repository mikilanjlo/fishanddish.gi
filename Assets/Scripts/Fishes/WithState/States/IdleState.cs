﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class IdleState : AState
{
    //TODO collection of curves
    private List<Fish> m_deadFishes;

    #region Public methods
    public IdleState(Fish fish) : base(fish)
    {
        fish.SetAliveFishSprite();
        this.m_deadFishes = new List<Fish>();
    }


    public override void Motion()
    {
        // TODO Motion
    }


    public override void TryToChangeState()
    {
        if (!Fish.IsAlive)
        {
            Fish.CurState = new DeadState(Fish);
            return;
        }
        if (Fish.FishType == EFishType.Predator) 
        {
            if (this.FindCaughtFishes())
            {
                //Можно передавать сразу в конструкторе хищной рыбы лист из пойманных рыб, чтобы она сразу летела уже на ближайшую, а там же и обновляла этот лист. 
                //для памяти будет оптимальней работа с листом и его очисткой, чем плодить кучу новых массивов
                Fish.CurState = new AggressiveState(Fish);
            }
        }
    }
    #endregion

    private bool FindCaughtFishes()
    {
        m_deadFishes.Clear();
        this.m_deadFishes.AddRange(Object.FindObjectsOfType<Fish>().Where(f => !f.IsAlive && f.FishType == EFishType.Base && f.FishSize == Fish.FishSize));
        return this.m_deadFishes.Count > 0;
    }
}