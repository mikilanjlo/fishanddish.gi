﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AState
{
	#region Fields
	protected Fish m_fish;
	#endregion

	#region Properties
	protected Fish Fish { get; set; }
	#endregion

	#region Public methods
	public AState(Fish fish)
	{
		Fish = fish;
	}
	

	public abstract void TryToChangeState();

	
	public abstract void Motion();
	#endregion
}
