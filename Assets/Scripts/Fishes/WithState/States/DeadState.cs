﻿public class DeadState : AState
{
	#region Public methods
    public DeadState(Fish fish) : base(fish)
    {
        fish.SetDeadFishSprite();
    }


    public override void Motion()
    {
        
    }


    public override void TryToChangeState()
    {
        
    }
    #endregion
}
