﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AggressiveState : AState 
{
    #region Public methods
    public AggressiveState(Fish fish) : base(fish)
    {
        // TODO turn on AgressiveSkin
    }


    public override void Motion()
    {
        // TODO Aggressive Motion
    }


    public override void TryToChangeState()
    {
        // TODO stop AggressState
    }
    #endregion
}
