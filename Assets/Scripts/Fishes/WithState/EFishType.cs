﻿public enum EFishType
{
	Base,       // Обычная
	Predator,   // Хищник
	Oxygen,     // Дающая кислород
	Jellyfish,  // Медуза
	Octopus,    // Осьминог
}