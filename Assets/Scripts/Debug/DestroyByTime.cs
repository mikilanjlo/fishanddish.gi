﻿using System.Collections;
using UnityEngine;

public class DestroyByTime : MonoBehaviour
{
    private float m_destroyTime = 3f;

    // Update is called once per frame
    private void Start()
    {
        StartCoroutine(this.Destroy());
    }

    private IEnumerator Destroy()
    {
        yield return new WaitForSeconds(this.m_destroyTime);

        Destroy(gameObject);
    }
}
