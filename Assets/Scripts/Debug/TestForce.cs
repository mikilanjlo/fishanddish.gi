﻿using UnityEngine;

public class TestForce : MonoBehaviour
{
    [SerializeField] private float force = 10f;
    private void Start()
    {
        var rb = GetComponent<Rigidbody2D>();
        rb.AddForce(transform.right*this.force, ForceMode2D.Impulse);
    }
}
