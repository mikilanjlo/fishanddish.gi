﻿using UnityEngine;

public class TestMoving : MonoBehaviour
{
    [SerializeField] private Transform m_destinationPoint;
    [SerializeField] private float m_speed = 0.3f;

    private Quaternion m_currentRotation = Quaternion.identity;
    private Fish m_fishRef;

    private void Start()
    {
        this.m_fishRef = GetComponent<Fish>();
        this.LookAt2D(m_destinationPoint.position);
    }

    private void Update()
    {
        if (this.m_fishRef.IsAlive)
        {
            transform.Translate(transform.right * this.m_speed * Time.deltaTime, Space.World);
        }
        
    }

    private void LookAt2D(Vector2 destPos)
    {
        var direction = destPos - (Vector2)transform.position;
        float angle = Vector2.Angle(transform.right, direction);

        if (angle > 0.01f)
        {
            Vector3 cross = Vector3.Cross(transform.right, direction);
            this.m_currentRotation *= Quaternion.AngleAxis(angle, cross);
            transform.rotation = this.m_currentRotation;
        }
    }
}
