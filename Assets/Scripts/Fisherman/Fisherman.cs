﻿using System.Collections.Generic;
using UnityEngine;

public class Fisherman : MonoBehaviour
{
    [SerializeField] private float m_minAngle = -30f;
    [SerializeField] private float m_maxAngle = 100f;
    [SerializeField] private float m_rotateSpeed = 10f;
    [SerializeField] private Transform m_harpoonTransform;
    [SerializeField] private Harpoon m_harpoonConfigFile;
    [SerializeField] private HarpoonShot m_harpoonShotPref;
    [SerializeField] private Target m_targetPref;

    private Target m_target;
    private Camera m_cam;

    //private Transform m_harpoonTransform;
    private Rigidbody2D m_harpoonRigidbody;

    private Quaternion m_defaultRotation;
    private Quaternion m_currentRotation;

    private HarpoonShot m_currentShot;
    private float m_harpoonShotSize;
    private float m_shootingTimer;
    private Vector2 m_shotDirection;

    private List<Fish> m_caughtFishes { get; set; }

    private void Awake()
    {
        this.m_target = Instantiate(this.m_targetPref, Vector3.zero, Quaternion.Euler(0, 0, 0));
        this.m_target.SetActiveTarget(false);
        //just for ScreenToWorldPoint method
        this.m_cam = Camera.main;

        this.m_defaultRotation = transform.rotation;
        this.m_currentRotation = transform.rotation;
    }

    private void Start()
    {
        this.m_caughtFishes = new List<Fish>();

        if (this.m_harpoonTransform == null)
        {
            Debug.LogError("Harpoon is missing!");
            return;
        }

        SpriteRenderer sr = this.m_harpoonTransform.GetComponent<SpriteRenderer>();
        if (sr != null)
        {
            sr.sprite = this.m_harpoonConfigFile.HarpoonSprite;
        }
        else
        {
            Debug.LogError("Harpoon hasn't got sprite renderer!");
        }

        this.m_harpoonRigidbody = this.m_harpoonTransform.GetComponent<Rigidbody2D>();
        if (this.m_harpoonRigidbody == null)
        {
            Debug.LogError("Harpoon hasn't got Rigidbody2D!");
        }

        if (this.m_harpoonShotPref != null)
        {
            this.m_harpoonShotPref.SetShotSprite(this.m_harpoonConfigFile.HarpoonShotSprite);
        }
        else
        {
            Debug.LogError("Harpoon Shot is missing!");
        }
    }
    
    private void Update()
    {
        this.TrySpawnShot();
        this.ShowTarget();
        this.TryShoot();
        this.DoRotation();
    }

    private void DoRotation()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 mousePositionGlobal = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            this.LookAt2D(mousePositionGlobal);
        }
        else
        {
            this.RotateToDefault();
        }
    }

    private void ShowTarget()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 mousePositionGlobal = this.m_cam.ScreenToWorldPoint(Input.mousePosition);
            if (!this.m_target.IsActive)
            {
                this.m_target.SetActiveTarget(true);
            }
            this.m_target.SetPositionAndSprite(mousePositionGlobal, 
                Vector2.Distance(this.m_harpoonTransform.position, mousePositionGlobal) <= this.m_harpoonConfigFile.RopeLength+this.m_harpoonShotSize);
        }
        else
        {
            if (this.m_target.IsActive)
            {
                this.m_target.SetActiveTarget(false);
            }
        }
    }

    private void RotateToDefault()
    {
        this.m_currentRotation = Quaternion.Lerp(this.m_currentRotation, this.m_defaultRotation, Time.deltaTime * this.m_rotateSpeed);
        this.m_harpoonTransform.rotation = this.m_currentRotation;
    }

    /// <summary>
    /// Rotate object id 2D so the right vetor points at target's current position
    /// </summary>
    /// <param name="mousePos">Mouse position in world space</param>
    private void LookAt2D(Vector2 mousePos)
    {
        this.m_shotDirection = mousePos - (Vector2)this.m_harpoonTransform.position;
        float angle = Vector2.Angle(this.m_harpoonTransform.right, this.m_shotDirection);

        if(angle > 0.01f)
        {
            Vector3 cross = Vector3.Cross(this.m_harpoonTransform.right, this.m_shotDirection);
            this.m_currentRotation *= Quaternion.AngleAxis(angle, cross);
            float currentZangle = this.m_currentRotation.eulerAngles.z;
            // EXAMPLE: in Unity -10 degree = 350 degree
            if (currentZangle > 180)
            {
                currentZangle -= 360;
            }
            if (currentZangle > this.m_maxAngle)
            {
                this.m_currentRotation.eulerAngles = new Vector3(this.m_currentRotation.eulerAngles.x, this.m_currentRotation.eulerAngles.y, this.m_maxAngle);
            }
            else if (currentZangle < this.m_minAngle)
            {
                this.m_currentRotation.eulerAngles = new Vector3(this.m_currentRotation.eulerAngles.x, this.m_currentRotation.eulerAngles.y, this.m_minAngle);
            }
            this.m_harpoonTransform.rotation = this.m_currentRotation;
        }
    }

    private void TrySpawnShot()
    {
        if (this.m_shootingTimer <= Time.time && (this.m_currentShot == null || this.m_currentShot.IsStoped))
        {
            this.m_currentShot = Instantiate(this.m_harpoonShotPref, this.m_harpoonTransform.position, this.m_harpoonTransform.rotation, this.m_harpoonTransform);
            this.m_currentShot.SetHarpoon();
            this.m_currentShot.Penetration = this.m_harpoonConfigFile.Penetration;
            this.m_currentShot.Damage = this.m_harpoonConfigFile.Damage;
            //Distance = ropeLen + shotSize
            if (this.m_harpoonShotSize <= 0)
            {
                var col = this.m_currentShot.gameObject.GetComponent<BoxCollider2D>();
                this.m_harpoonShotSize = col.size.x;
            }

            //Distance joint for max rope length
            DistanceJoint2D joint = this.m_currentShot.GetComponent<DistanceJoint2D>();
            joint.anchor = this.m_currentShot.ConnectionPoint.localPosition;
            joint.connectedAnchor = Vector2.zero;
            joint.connectedBody = this.m_harpoonRigidbody;
            joint.distance = this.m_harpoonConfigFile.RopeLength;
        }
    }

    private void TryShoot()
    {
        if (Input.GetMouseButtonUp(0) && this.m_currentShot.CanShoot)
        {
            this.m_currentShot.Shoot(this.m_harpoonConfigFile.ShotForce);
            this.m_shootingTimer = Time.time + this.m_harpoonConfigFile.ShotDelay;
        }
    }

    public void AddFishes(List<Fish> fishes)
    {
        this.m_caughtFishes.AddRange(fishes);
        foreach (Fish fish in fishes)
        {
            WaterSceneManager.Instance.Gold += fish.Cost;
            WaterSceneManager.Instance.Count++;
        }
    }
}
