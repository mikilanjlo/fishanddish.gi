﻿using System.Collections.Generic;
using UnityEngine;

public class Rope : MonoBehaviour
{
    //[SerializeField] private float m_distance = 0.3f;
    [SerializeField] private GameObject m_nodePref;
    private List<Transform> m_nodePoints = new List<Transform>();

    private Transform m_harpoon;
    //private GameObject m_lastNode;
    private HarpoonShot m_shot;
    private LineRenderer m_lineRenderer;
    //private bool m_done = false;
    private int vertexCount = 2;

    //private Vector2 m_prevPos;

    private void Start()
    {
        this.m_lineRenderer = GetComponent<LineRenderer>();
        this.m_shot = GetComponent<HarpoonShot>();

        this.m_harpoon = this.m_shot.Harpoon;
        //this.m_prevPos = this.m_harpoon.transform.position;
        this.m_nodePoints.Add(this.m_harpoon.transform);
    }

    private void Update()
    {
        //if (!this.m_shot.IsStoped && ! this.m_done)
        //{
        //    if (Vector2.Distance(this.m_prevPos, this.m_shot.ConnectionPoint.position) >= this.m_distance)
        //    {
        //        this.CreateNode();
        //    }
        //}
        //if (this.m_shot.IsStoped && !this.m_done)
        //{
        //    for (int i = 1; i < this.m_nodePoints.Count; i++)
        //    {
        //        this.m_nodePoints[i].SetParent(transform);
        //    }
        //}


        this.RenderLine();
    }

    private void RenderLine()
    {
        this.m_lineRenderer.positionCount = this.vertexCount;

        for (int i = 0; i < this.m_nodePoints.Count; i++)
        {
            this.m_lineRenderer.SetPosition(i, this.m_nodePoints[i].position);
        }

        this.m_lineRenderer.SetPosition(this.m_nodePoints.Count, this.m_shot.ConnectionPoint.position);
    }

    //private void CreateNode()
    //{
    //    GameObject node = Instantiate(this.m_nodePref, this.m_shot.ConnectionPoint.position, Quaternion.identity);
    //    this.m_nodePoints.Add(node.transform);
    //    this.m_prevPos = node.transform.position;

    //    if (this.m_nodePoints.Count == 2)
    //    {
    //        node.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
    //    }

    //    this.vertexCount++;
    //}
}
