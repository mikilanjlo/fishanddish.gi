﻿using UnityEngine;

[CreateAssetMenu(fileName = "HarpoonObj", menuName = "Harpoon")]
public class Harpoon : ScriptableObject
{
    [SerializeField] private string m_name;
    [SerializeField] private float m_ropeLength;
    [SerializeField] private float m_penetration;
    [SerializeField] private float m_damage;
    [SerializeField] private float m_shotForce;
    [SerializeField] private float m_shotDelay;
    [SerializeField] private Sprite m_harpoonSprite;
    [SerializeField] private Sprite m_harpoonShot;


    public string Name
    {
        get { return this.m_name; }
    }

    public float RopeLength
    {
        get { return this.m_ropeLength; }
    }

    public float Penetration
    {
        get { return this.m_penetration; }
    }

    public float Damage
    {
        get { return this.m_damage; }
    }

    public float ShotForce
    {
        get { return this.m_shotForce; }
    }

    public float ShotDelay
    {
        get { return this.m_shotDelay; }
    }

    public Sprite HarpoonSprite
    {
        get { return this.m_harpoonSprite; }
    }

    public Sprite HarpoonShotSprite
    {
        get { return this.m_harpoonShot; }
    }
}
