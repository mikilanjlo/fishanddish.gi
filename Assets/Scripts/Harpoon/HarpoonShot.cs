﻿using System.Collections.Generic;
using UnityEngine;

//Physic of shot
public class HarpoonShot : MonoBehaviour
{
    [SerializeField] private SpriteRenderer m_renderer;
    [SerializeField] private Transform m_connectionPoint;
    [SerializeField] private float m_backSpeed = 3f;
    
    private Rigidbody2D m_rigidbody;
    private List<Fish> m_caughtFishes;

    #region Settings
    public Transform Harpoon { get; private set; }

    public float Penetration { get; set; }
    
    public float Damage { get; set; }

    public float ShotForce { get; set; }

    public bool IsStoped { get; private set; }

    public bool CanShoot { get; private set; } = true;
    
    public Transform ConnectionPoint
    {
        get
        {
            if (this.m_connectionPoint == null)
            {
                Debug.LogError("ConnectionPoint transform on Harpoon Shot is missing!");
            }
            
            return this.m_connectionPoint;
        }
    }

    #endregion

    private void Start()
    {
        this.m_caughtFishes = new List<Fish>();
        this.m_rigidbody = GetComponent<Rigidbody2D>();
        if (this.m_rigidbody == null)
        {
            Debug.LogError("Rigidbidy2D on Harpoon Shot is missing!");
        }
    }

    public void SetHarpoon()
    {
        this.Harpoon = transform.parent;
    }

    public void SetShotSprite(Sprite sprite)
    {
        if (this.m_renderer != null)
        {
            this.m_renderer.sprite = sprite;
        }
        else
        {
            Debug.LogError("Sprite renderer on Harpoon Shot is missing!");
        }
    }

    public void Shoot(float force)
    {
        transform.parent = null; //move shot to global space

        this.m_rigidbody.bodyType = RigidbodyType2D.Dynamic;
        this.m_rigidbody.AddForce(transform.right * force, ForceMode2D.Impulse);
        this.CanShoot = false;
    }

    private void Update()
    {
        if (this.m_rigidbody.bodyType == RigidbodyType2D.Kinematic) return;

        if (this.m_rigidbody.velocity.magnitude < 0.1f && !this.IsStoped)
        {
            this.IsStoped = true;
            this.OnShotStopped();
        }

        if (this.IsStoped)
        {
            Vector2 dir = this.Harpoon.position - transform.position;
            transform.Translate(dir * this.m_backSpeed * Time.deltaTime, Space.World);
        }
    }

    private void OnShotStopped()
    {
        this.m_renderer.color = new Color(this.m_renderer.color.r, this.m_renderer.color.g, this.m_renderer.color.b, 0.6f);
        gameObject.AddComponent<DestroyByTime>();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        Fish fish = other.gameObject.GetComponent<Fish>();
        if (fish != null)
        {
            if (this.m_rigidbody.velocity.magnitude * this.Penetration >= fish.PunchAbility)
            {
                fish.Health -= this.Damage;
                if (!fish.IsAlive)
                {
                    this.m_rigidbody.velocity -= this.m_rigidbody.velocity / 2;
                    fish.gameObject.GetComponent<Collider2D>().isTrigger = true;
                    fish.transform.SetParent(transform);
                    this.m_caughtFishes.Add(fish);
                    Debug.Log("Fish has been caught");
                }
            }
            else
            {
                Debug.Log("Can't catch fish. Punch ability is higer than penetration power.");
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (this.CanShoot && !this.IsStoped) return;
        
        Fisherman fisherman = collider.GetComponent<Fisherman>();
        if (fisherman != null)
        {
            if (this.m_caughtFishes.Count != 0)
            {
                fisherman.AddFishes(this.m_caughtFishes);
                Debug.Log("Fisherman got some fishes!");
            }

            Debug.Log("Fisherman got some fishes!");
            Destroy(gameObject);
        }
    }
}
