﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Target : MonoBehaviour
{
    [SerializeField] private Sprite m_targetSprite;
    [SerializeField] private Sprite m_targetFarSprite;

    private SpriteRenderer m_spriteRenderer;

    public bool IsActive
    {
        get { return gameObject.activeSelf; }
    }

	private void Awake ()
	{
	    this.m_spriteRenderer = GetComponent<SpriteRenderer>();
        gameObject.SetActive(false);
	}

    public void SetActiveTarget(bool show)
    {
        gameObject.SetActive(show);
    }

    public void SetPositionAndSprite(Vector2 position, bool canCatch)
    {
        transform.position = position;
        this.TryChangeSprite(canCatch ? this.m_targetSprite : this.m_targetFarSprite);
    }

    private void TryChangeSprite(Sprite sprite)
    {
        if (this.m_spriteRenderer.sprite != sprite)
        {
            this.m_spriteRenderer.sprite = sprite;
        }
    }
}
