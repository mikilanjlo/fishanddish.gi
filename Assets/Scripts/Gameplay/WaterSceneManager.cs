﻿using UnityEngine;
using UnityEngine.UI;

public class WaterSceneManager : MonoBehaviour
{
    [SerializeField] private Text m_goldText;
    [SerializeField] private Text m_countText;

    private const string GOLD_PATTERN = "Gold: {0:F2}";
    private const string COUNT_PATTERN = "Count: {0}";
    private float m_gold = 0f;
    private int m_count = 0;

    public float Gold
    {
        get { return m_gold; }
        set
        {
            this.m_gold = value;
            this.m_goldText.text = string.Format(GOLD_PATTERN, value);
        }
    }

    public int Count
    {
        get { return this.m_count; }
        set
        {
            this.m_count = value;
            this.m_countText.text = string.Format(COUNT_PATTERN, value);
        }
    }

    public static WaterSceneManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        //TODO сделать ScriptableObject, который будет хранить всех пойманных рыб, их количество, текущее количество голды и т.п.
        this.Gold = 0; // присваивать голду из настроечки
        this.Count = 0;
    }
}
